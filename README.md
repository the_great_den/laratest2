<div aria-hidden="false" class="rah-static rah-static--height-auto" style="height: auto; overflow: visible;"><div><article data-qa="readme-container" class="css-1q7i31j evb0awb0"><div><h1 id="markdown-header-home-task">Home task</h1>
<ol>
<li>
<strike>
Clone <a data-is-external-link="true" href="https://bitbucket.org/yevhen-turovets/laravel-2/src" rel="nofollow">this</a> repository
</strike>
</li>
<li><strike>
Install &amp; run the project locally or in the container with <a data-is-external-link="true" href="https://dotsandbrackets.com/quick-intro-to-docker-compose-ru/" rel="nofollow">docker-compose</a>
A few examples of how to execute common commands in docker-compose:</strike><ul>
<li>Install composer-package <code>docker-compose exec app composer require package/name</code></li>
<li>Execute <em>php artisan</em> commands <code>docker-compose exec app php artisan ...</code></li>
</ul>
</li>
<li><strike>Create a database, run migrations and seed database with data.</strike></li>
<li>
<p><strike>Add the following Entities with relations and migrations:</strike></p>
<ul>
<li>Order <code>[id, date, orderItems, buyer]</code></li>
<li>OrderItem <code>[id, order, product, quantity, price, discount (%), sum (price x discount x quantity)]</code></li>
<li>Buyer <code>[id, name, surname, country, city, addressLine, phone, orders]</code></li>
</ul>
<strike>
<p>relations:
- Order -&gt; OrderItem(many), 
- Buyer -&gt; Orders(many)</p>
<p><em>all money values should be stored in cents - 5$ =&gt; 500 in db</em>
</strike>
<br> <strike>
5. Create factories for <strong>Order, OrderDetail, Buyer</strong> using Faker<br>
6. Create order seeder which will create Order with new OrderItems, Buyer using factories and already existing in database Products <br>
7. Add CRUD endpoints (API routes and controller actions) for Order entity:
- create order shape:</strike>
</p><div class="codehilite"><pre><span></span><span class="c">POST: {</span>
<span class="c">    buyerId: id,</span>
<span class="c">    orderItems: [{</span>
<span class="c">        productId: id.</span>
<span class="c">        productQty: quantity,</span>
<span class="c">        productDiscount: %,</span>
<span class="c">    }, { </span>
<span class="c">        ...</span>
<span class="c">    }]</span>
<span>}</span>
</pre></div>
<strike>- update order shape:</strike>
<div class="codehilite"><pre><span></span><span class="c">PUT: {</span>
<span class="c">    orderId: id,</span>
<span class="c">    orderItems: [{</span>
<span class="c">        productId: id.</span>
<span class="c">        productQty: quantity,</span>
<span class="c">        productDiscount: %,</span>
<span class="c">    }, { </span>
<span class="c">        ...</span>
<span class="c">    }]</span>
<span>}</span>
</pre></div><p></p>
</li>
<li>
<p><strike>Create resource presenters and return Order by id in the following shape: </strike>
</p><div class="codehilite"><pre><span></span><span>{ </span>
<span>    data: {</span>
<span>        orderId: id,</span>
<span>        orderDate: date,</span>
<span>        orderSum: sum, </span>
<span>        orderItems: [{</span>
<span>            productName: name,</span>
<span>            productQty: quantity,</span>
<span>            productPrice: price,</span>
<span>            productDiscount: %,</span>
<span>            productSum</span>
<span>        }, { </span>
<span>            ... </span>
<span>        }], </span>
<span>        buyer: {</span>
<span>            buyerFullName: name+surname, </span>
<span>            buyerAddress: country+city+addressLine,</span>
<span>            buyerPhone: phone</span>
<span>        }</span>
<span>    }</span>
<span>}</span>
</pre></div><p></p>
</li>
</ol></div></article></div></div>
