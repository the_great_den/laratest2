<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $quantity = random_int(1, 50);
        $discount = random_int(0, 50);
        $price = random_int(100, 9950);

        return [
            'quantity' => $quantity,
            'price' => $price,
            'discount' => $discount,
        ];
    }
}
