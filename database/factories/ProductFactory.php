<?php
namespace Database\Factories;
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    public function definition()
    {
        return [
            'name' => $this->faker->text($maxNbChars = 10),
            'price' => $this->faker->numberBetween(100, 10000),
            'available' => $this->faker->boolean()
        ];
    }
}
