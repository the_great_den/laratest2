<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Seller, Product};

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sellers = Seller::factory(20)->create()->each(function($seller) {
            $seller->products()->saveMany(
               Product::factory(20)->create([
                   'seller_id' => $seller->id
               ])
            );
        });
    }
}
