<?php
namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use App\Models\{Order, OrderItem, Buyer, Products};

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Buyer -> Orders(many) -> OrderItem(many)
     * Create order seeder which will create Order with new OrderItems, Buyer using factories and already existing in database Products
     * @return void
     */
    public function run()
    {
        Order::factory()->create(['buyers_id' => null, 'order_items_id' => null])->each(function($order) {
            $buyer = Buyer::factory()->create(['orders_id' => $order->id]);
            $items = OrderItem::factory(random_int(2, 12))->create([
                'orders_id' => $order->id,
                'my_products_id' => null,
            ])->each(function($orderItem){
                $orderItem->my_products_id = Product::all()->random()->id;
                $orderItem->save();
            });
            $order->buyers_id = $buyer->id;
            $order->order_items_id = $items->random()->id;
            $order->save();
        });
    }
}
