<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationsOrderitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->foreignId('orders_id')->onDelete('cascade')->nullable();
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders')
                ->nullable();
            $table->foreignId('my_products_id')->nullable();
            $table->foreign('my_products_id')
                ->references('id')
                ->on('my_products')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropForeign('order_items_orders_id_foreign');
            $table->dropForeign('order_items_my_products_id_foreign');
            $table->dropColumn(['orders_id','my_products_id']);
        });
    }
}
