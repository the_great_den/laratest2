<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationsInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->foreignId('buyers_id')->onDelete('cascade')->nullable();
            $table->foreign('buyers_id')
                ->references('id')
                ->on('buyers')
                ->nullable();
            $table->foreignId('order_items_id')->onDelete('cascade')->nullable();
            $table->foreign('order_items_id')
                ->references('id')
                ->on('order_items')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_buyers_id_foreign');
            $table->dropForeign('orders_order_items_id_foreign');
            $table->dropColumn(['buyers_id','order_items_id']);
        });
    }
}
