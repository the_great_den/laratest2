<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = ['id'];

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function setSumAttribute($value){
        $this->attributes['sum'] = $value * 100;
    }

    public function getSumAttribute($value){
        return $value / 100;
    }

    public function setPriceAttribute($value){
        $this->attributes['price'] = $value * 100;
    }

    public function getPriceAttribute($value){
        return $value / 100;
    }

}
