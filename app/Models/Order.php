<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function buyer() {
        return $this->belongsTo(Buyer::class, 'buyers_id', 'id');
    }

    public function orderItems(){
        return $this->hasMany(OrderItem::class, 'orders_id', 'id');
    }

    public function sum() {
        return ($this->price * 0.01 * (100 - $this->discount)) * $this->quantity;
    }

}
