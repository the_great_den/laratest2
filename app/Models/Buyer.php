<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = ['id'];

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
