<?php

namespace App\Presenters;


use App\Models\Order;
use App\Models\Product;

class OrderPresenter
{
    protected $model;

    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->model, $method], $args);
    }

    public function __get($name)
    {
        return $this->model->{$name};
    }



    public function getProducts()
    {
        $data = [
            'orderItems' => [],
            'total_sum' => 0
        ];

        foreach ($this->model->orderItems as $item) {
            $product = Product::find($item->my_products_id);
            $sum = $product->price * $item->quantity * 0.01 * (100 - $item->discount);
            $data['orderItems'][] = [
                'productName' => $product->name,
                'productQty' => $item->quantity,
                'productPrice' => $product->price,
                'productDiscount' => $item->discount,
                'productSum' => number_format($sum, 2, ',', ' ' ),
            ];
            $data['total_sum'] += $sum;

            return $data;
        }

    }

    public function orderId()
    {
        return $this->model->id;
    }

    public function orderDate()
    {
        return $this->model->date;
    }

    public function orderSum()
    {
        $data = $this->getProducts();
        return number_format($data['total_sum'], 2, ',', ' ' );
    }

    public function orderItems()
    {
        $data = $this->getProducts();
        return $data['orderItems'];
    }
}
