<?php

namespace App\Presenters;


use App\Models\Buyer;


class BuyerPresenter
{
    protected $model;

    public function __construct(Buyer $model)
    {
        $this->model = $model;
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->model, $method], $args);
    }

    public function __get($name)
    {
        return $this->model->{$name};
    }

    public function buyerFullName()
    {
        return $this->model->name.' '.$this->model->surname;
    }

    public function buyerAddress()
    {
        return $this->model->country.' '.$this->model->city.' '.$this->model->addressLine;
    }

    public function buyerPhone()
    {
        return $this->model->phone;
    }

}
