<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use App\Presenters\BuyerPresenter;
use App\Presenters\OrderPresenter;
use Illuminate\Http\Request;


class OrdersController extends Controller
{
    public function GetData($id){
        $order = Order::findOrFail($id);
        $order = new OrderPresenter($order);
        $buyer = new BuyerPresenter($order->buyer);

        return [
            'orderId' => $order->orderId(),
            'orderDate' => $order->orderDate(),
            'orderSum' => $order->orderSum(),
            'orderItems' => $order->orderItems(),
            'buyer' => [
                'buyerFullName' => $buyer->buyerFullName(),
                'buyerAddress' => $buyer->buyerAddress(),
                'buyerPhone' => $buyer->buyerPhone(),
            ]
        ];
    }

    public function index()
    {
        $orders = Order::all();
        return view('orders', compact('orders'));
    }

    public function store(Request $request)
    {
        $order = Order::create([
            'buyer_id' => $request->buyerId,
        ]);

        foreach($request->orderItems as $item)
        {
            OrderItem::create([
                'orders_id' => $order->id,
                'my_products_id' => $request->productId,
                'quantity' => $request->productQty,
                'discount' => $request->productDiscount,
            ]);
        }

        return json_encode($this->GetData($order->id));
    }

    public function show($id)
    {
        return json_encode($this->GetData($id));
    }

    public function update(Request $request, $id)
    {

        foreach($request->orderItems as $item)
        {
            OrderItem::where('id', $item->id)->update([
                'orders_id' => $request->id,
                'my_products_id' => $request->productId,
                'quantity' => $request->productQty,
                'discount' => $request->productDiscount,
            ]);
        }

        return json_encode($this->GetData($id));
    }
}
